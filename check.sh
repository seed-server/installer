
source "$(dirname $0)/settings.sh"


DATABASE_SETUP "check.csv" "app;name;result;"

#--------------------------------------------
function CHECK(){
#--------------------------------------------

    local name="$1"
    shift 1
    local command="$@"

    echo
    echo $app $name
    echo
    $command
    result=$?

    DATABASE_ADD "$APP;$name;$result;"
}
#--------------------------------------------



for script in $(find $WORKSPACE -type f -name "__check__.sh")
do
   export APP="$(dirname $script)"
    source $script
done


