
#--------------------------------------------
function DATABASE_SETUP(){
#--------------------------------------------
    export DATABASE="$WORKSPACE/.database"
    mkdir -p "$DATABASE"
    export RECORD="$DATABASE/$1"
    if [ ! -f "$RECORD" ]
    then
        echo "$2" > "$RECORD"
    fi
}
#--------------------------------------------
function DATABASE_ADD(){
#--------------------------------------------

    echo "$1" >> "$RECORD"
}
#--------------------------------------------
