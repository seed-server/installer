
if [ -z "$SEED_MODE" ]
then
    export GIT_REPO="https://framagit.org/seed-apps"
else
    export GIT_REPO="git@framagit.org:seed-apps"
fi



export INSTALLER="$(realpath $(dirname $0))"
export WORKSPACE="$(realpath $INSTALLER/..)"


for library in $(find $INSTALLER/__bash -type f -name "*.sh")
do
    source $library
done
