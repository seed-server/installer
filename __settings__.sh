

function @install() {
    bash $WORKSPACE/installer/install.sh $@
}


function @check() {
    bash $WORKSPACE/installer/check.sh $@
}

function @update() {
    bash $WORKSPACE/installer/update.sh $@
}


